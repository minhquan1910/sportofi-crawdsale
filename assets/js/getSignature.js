async function getSignature() {
    const { ethereum } = window;
    

    if (ethereum && ethereum.isMetaMask) {
        const provider = new ethers.providers.Web3Provider(window.ethereum)
        const paymentToken = '0xe9e7cea3dedca5984780bafc599bd69add087d56'.toLowerCase();
        const signer = provider.getSigner()
        const amountInput = document.getElementById('amount').value;
        const amount = ethers.utils.parseEther(amountInput?.toString());
        const commandGateAddress = "0xc470f0e92496B2F6706c435AD50a43ddf85F7b99";
        const fnSig = "0xf8af8456";
        const contract = "0x9dC83E6977332389687cEc7265201880eB0A026d"
        const data = "0x";
        const minVolumn = 1000;
        const maxVolumn = 100000;
        console.log("A")
        if (window.localStorage.getItem('account') != null){
            if (parseInt(amountInput) >= minVolumn && parseInt(amountInput) <= maxVolumn){
                // document.getElementById('token-volumn').textContent = tokenVolumnNumber;
                const ABI = ['function approve(address cmdGate, uint256 amount) returns (bool)']
                let tokenContract = new ethers.Contract(
                    paymentToken,
                    ABI,
                    signer,
                );
        
                const tokenContractWithSigner = tokenContract.connect(signer)
                
                try {
                    let tx = await tokenContractWithSigner.approve(commandGateAddress, amount)
                    const receiptApprove = await tx.wait()
                    // console.log(receiptApprove)
        
                    const cmdGateABI = ['function depositERC20WithCommand(address token, uint256 value, bytes4 fnSig, address contract, bytes memory data)']
                    let cmdGate = new ethers.Contract(
                        commandGateAddress,
                        cmdGateABI,
                        signer,
                    )
        
                    let depositTx = await cmdGate.connect(signer).depositERC20WithCommand(paymentToken, amount, fnSig, contract, data)
                    console.log(depositTx)
        
                    const receiptDeposit = await depositTx.wait();
                    location.reload();
                } catch (err) {
                    console.log(err)
                }
                // 
            }
        }
        location.reload();
    } else {
        // if (typeof window.ethereum != 'underfined'){
        //     if (window.localStorage.getItem('account') == null && ethereum.isConnected()){
        //         await provider.send("eth_requestAccounts", []);
        //         const signer = provider.getSigner()
                
        //         document.getElementById('disconnectBtn').style.display = "block";
        //         document.getElementById('connectBtn').style.display = "none";
        //         document.getElementById('accountBtn').style.display = "block";
        //         document.getElementById('account').textContent = (await signer.getAddress()).substring(0, 12) + '...';
        //         window.localStorage.setItem("account", (await signer.getAddress()))
        //     }
        //     else if (ethereum.isConnected()){
        //         await provider.send("wallet_requestPermissions", []);
        //         document.getElementById('disconnectBtn').style.display = "block";
        //         document.getElementById('connectBtn').style.display = "none";
        //         document.getElementById('accountBtn').style.display = "block";
        //         document.getElementById('account').textContent = (await signer.getAddress()).substring(0, 12) + '...';
        //         window.localStorage.setItem("account", (await signer.getAddress()))
        //     }
        // } else{
            console.log("B")
            window.location.href = 'https://metamask.app.link/dapp/crowdsale.sportofi.com/';
        }
    }


