// connect wallet and change UI

async function connectWallet() {
    // window.location.href = 'https://metamask.app.link/dapp/crowdsale.sportofi.com/'
    const { ethereum } = window;
    if (ethereum && ethereum.isMetaMask) {
        console.log("A")
        const provider = new ethers.providers.Web3Provider(window.ethereum)

        if (window.localStorage.getItem('account') == null && ethereum.isConnected()){
            await provider.send("eth_requestAccounts", []);
            const signer = provider.getSigner()
            
            document.getElementById('disconnectBtn').style.display = "block";
            document.getElementById('connectBtn').style.display = "none";
            document.getElementById('accountBtn').style.display = "block";
            document.getElementById('account').textContent = (await signer.getAddress()).substring(0, 12) + '...';
            window.localStorage.setItem("account", (await signer.getAddress()))
        }
        else if (ethereum.isConnected()){
            await provider.send("wallet_requestPermissions", []);
            document.getElementById('disconnectBtn').style.display = "block";
            document.getElementById('connectBtn').style.display = "none";
            document.getElementById('accountBtn').style.display = "block";
            document.getElementById('account').textContent = (await signer.getAddress()).substring(0, 12) + '...';
            window.localStorage.setItem("account", (await signer.getAddress()))
        }
        location.reload();

    } else {
        console.log("B")
        window.location.href = 'https://metamask.app.link/dapp/crowdsale.sportofi.com/';
    }
    // if (typeof window.ethereum != 'underfined'){
    //     
    // } else{
    //     
    // }
}
