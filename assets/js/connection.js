// make sure when reload page not disconnected wallet

document.addEventListener('DOMContentLoaded', async function() {
    account = window.localStorage.getItem('account')

    if (account != null) {
        // for loop because textContent cant put mutiple in the same time
        document.getElementById('disconnectBtn').style.display = "block";
        document.getElementById('connectBtn').style.display = "none";
        document.getElementById('accountBtn').style.display = "block";
        document.getElementById('account').textContent = account.substring(0, 12) + "...";

        const provider = new ethers.providers.Web3Provider(window.ethereum)
        const vestingAddress = "0x9dC83E6977332389687cEc7265201880eB0A026d";
        const vestingABI = ['function schedules(address) external view returns (uint96, uint96)',
                            'function vestingTotal() external view returns (uint96)'];
        const signer = provider.getSigner();
        const totalToken = 240000000;
        const price = 0.007;
        let vestingContract = new ethers.Contract(
            vestingAddress,
            vestingABI,
            signer,
        );
        console.log(vestingContract)

        const vestingObject = await vestingContract.schedules(await signer.getAddress());
        const tokenVolumn = ethers.utils.formatEther((vestingObject[0]._hex))
        const tokenVolumnNumber = parseFloat(parseFloat(tokenVolumn).toFixed(4))
        window.localStorage.setItem('tokenVolumn', tokenVolumnNumber.toLocaleString("en-US"));

        // console.log(tokenVolumnNumber.toLocaleString("en-US", {style:"currency", currency:"USD"}));
        document.getElementById('token-volumn').textContent = window.localStorage.getItem('tokenVolumn');

        const vestingTotal = await vestingContract.vestingTotal();
        const vestingTotalNumber = ethers.utils.formatEther((vestingTotal._hex))
        console.log(vestingTotalNumber)
        const raisedVolumn = ((vestingTotalNumber*price) + 690000).toLocaleString("en-US", {style:"currency", currency:"USD"});
        window.localStorage.setItem('raisedVolumn', raisedVolumn);
        //parseFloat(parseFloat(vestingTotalNumber).toFixed(4))
        console.log(raisedVolumn)
        
        document.getElementById('raised-volumn').textContent = window.localStorage.getItem('raisedVolumn');
        const value = ((vestingTotalNumber*price + 960000)/(vestingTotalNumber*price + 960000 + 1680000))*100;
        // console.log(value);
        window.localStorage.setItem('progress', value.toString());
        

        document.getElementById('progress').style.width = value.toString() + "%";

    }
    else {
        document.getElementById('account').style.display = "none";
        document.getElementById('disconnectBtn').style.display = "none";
    }
});