// disconnect
async function disconnect() {
    if (window.localStorage.length != 0){
        document.getElementById('connectBtn').style.display = "block";
        document.getElementById('disconnectBtn').style.display = "none";
        document.getElementById('accountBtn').style.display = "none";
        window.localStorage.removeItem("account");
        // await window.ethereum.request({
        //     method: "eth_requestAccounts",
        //     params: [{eth_accounts: {}}]
        // })
    }
}